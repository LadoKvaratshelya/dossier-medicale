\connect dossier_medical_centralise;

create table personne (
	id int primary key,
        nom text,
	prenom text,
	dateDeNaissance date
);

create table personnel (
	id int primary key,
        id_p int foreign key referencing personne,
        codeEmploye text
);
