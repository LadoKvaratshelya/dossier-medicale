/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class PersonnelSante extends Personnel{
    
    public PersonnelSante(String nom, String prenom, Date dateDeNaissance, String codeEmploye) {
        super(nom, prenom, dateDeNaissance, codeEmploye);
    }

    public String getCodeEmploye() {
        return codeEmploye;
    }

    public void setCodeEmploye(String codeEmploye) {
        this.codeEmploye = codeEmploye;
    }
    
}
