/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class DossierMedical {
    
    private Patient patient;
    private ArrayList<Antecedent> antecedents;
    private ArrayList<Visite> visites;

    public DossierMedical(Patient patient, ArrayList<Antecedent> antecedents, ArrayList<Visite> visites) {
        this.patient = patient;
        this.antecedents = antecedents;
        this.visites = visites;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public ArrayList<Antecedent> getAntecedents() {
        return antecedents;
    }

    public void setAntecedents(ArrayList<Antecedent> antecedents) {
        this.antecedents = antecedents;
    }

    public ArrayList<Visite> getVisites() {
        return visites;
    }

    public void setVisites(ArrayList<Visite> visites) {
        this.visites = visites;
        if (visites != null){
        Visite lastVisite = this.visites.get(this.visites.size()-1);
            if (lastVisite.isEstPremiereVisite()){
                ajouterAntecedent(lastVisite);
            }
            if (lastVisite.isEstVisiteFinDeTraitement() && antecedents != null){
                Antecedent antecedent = this.antecedents.get(this.antecedents.size()-1);
                finAntecedent(lastVisite, antecedent);
            }
        }
    }

    private void ajouterAntecedent(Visite visite) {
        Antecedent antecedent = new Antecedent(visite.getMedecin(), visite.getTraitement(), visite.getDiagnostique(), visite.getDateVisite(), null);
        this.antecedents.add(antecedent);
    } 
    
    private void finAntecedent(Visite visite, Antecedent antecedent) {
        antecedent.setDateFinTraitement(visite.getDateVisite());
    }
    
}
