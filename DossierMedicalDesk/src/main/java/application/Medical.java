/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Medical extends Traitement{
    
    private ArrayList<Medicament> medicaments;

    public Medical(ArrayList<Medicament> medicaments) {
        this.medicaments = medicaments;
    }

    public Medical() {
    }

    public ArrayList<Medicament> getMedicaments() {
        return medicaments;
    }

    public void setMedicaments(ArrayList<Medicament> medicaments) {
        this.medicaments = medicaments;
    }
    
    
}
