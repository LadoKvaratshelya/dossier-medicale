/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
class Antecedent {
    
    private Medecin medecin;
    private Traitement traitement;
    private String diagnostique;
    private Date dateDebutTraitement;
    private Date dateFinTraitement;

    public Antecedent(Medecin medecin, Traitement traitement, String diagnostique, Date dateDebutTraitement, Date dateFinTraitement) {
        this.medecin = medecin;
        this.traitement = traitement;
        this.diagnostique = diagnostique;
        this.dateDebutTraitement = dateDebutTraitement;
        this.dateFinTraitement = dateFinTraitement;
    }

    public Medecin getMedecin() {
        return medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public Traitement getTraitement() {
        return traitement;
    }

    public void setTraitement(Traitement traitement) {
        this.traitement = traitement;
    }

    public String getDiagnostique() {
        return diagnostique;
    }

    public void setDiagnostique(String diagnostique) {
        this.diagnostique = diagnostique;
    }

    public Date getDateDebutTraitement() {
        return dateDebutTraitement;
    }

    public void setDateDebutTraitement(Date dateDebutTraitement) {
        this.dateDebutTraitement = dateDebutTraitement;
    }

    public Date getDateFinTraitement() {
        return dateFinTraitement;
    }

    public void setDateFinTraitement(Date dateFinTraitement) {
        this.dateFinTraitement = dateFinTraitement;
    }

   
    
    
}
