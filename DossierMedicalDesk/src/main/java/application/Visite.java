/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Visite {
    
    private Etablissement etablissement;
    private Medecin medecin;
    private Date dateVisite;
    private String diagnostique;
    private Traitement traitement;
    private String resume;
    private Note[] notes;
    private boolean estVisiteFinDeTraitement;
    private boolean estPremiereVisite;

    public Visite(Etablissement etablissement, Medecin medecin, Date dateVisite, String diagnostique, String resume, boolean finTraitement, boolean estPremiereVisite) {
        this.etablissement = etablissement;
        this.medecin = medecin;
        this.dateVisite = dateVisite;
        this.diagnostique = diagnostique;
        this.resume = resume;
        estVisiteFinDeTraitement = finTraitement;
        this.estPremiereVisite = estPremiereVisite;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    public Medecin getMedecin() {
        return medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public Date getDateVisite() {
        return dateVisite;
    }

    public void setDateVisite(Date dateVisite) {
        this.dateVisite = dateVisite;
    }

    public String getDiagnostique() {
        return diagnostique;
    }

    public void setDiagnostique(String diagnostique) {
        this.diagnostique = diagnostique;
    }

    public Traitement getTraitement() {
        return traitement;
    }

    public void setTraitement(Traitement traitement) {
        this.traitement = traitement;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Note[] getNotes() {
        return notes;
    }

    public void setNotes(Note[] notes) {
        this.notes = notes;
    }

    public boolean isEstVisiteFinDeTraitement() {
        return estVisiteFinDeTraitement;
    }

    public void setEstVisiteFinDeTraitement(boolean estVisiteFinDeTraitement) {
        this.estVisiteFinDeTraitement = estVisiteFinDeTraitement;
    }

    public boolean isEstPremiereVisite() {
        return estPremiereVisite;
    }

    public void setEstPremiereVisite(boolean estPremiereVisite) {
        this.estPremiereVisite = estPremiereVisite;
    }
    
}
