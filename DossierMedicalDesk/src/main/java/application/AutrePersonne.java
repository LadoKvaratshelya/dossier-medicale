/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import application.Personne;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class AutrePersonne extends Personne{
    Genre genre;
    Coordonnees coordonnees;


    public AutrePersonne(Genre genre, Coordonnees coordonnees, String nom, String prenom, Date dateDeNaissance) {
        super(nom, prenom, dateDeNaissance);
        this.genre = genre;
        this.coordonnees = coordonnees;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Coordonnees getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(Coordonnees coordonnees) {
        this.coordonnees = coordonnees;
    }
    
    
}
