/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Medecin extends Personnel{
    
    String numPermis;

    public Medecin(String codeEmploye, String numPermis, String nom, String prenom, Date dateDeNaissance) {
        super(nom, prenom, dateDeNaissance, codeEmploye);
        this.numPermis = numPermis;
        
    }
    
    

    public String getNumPermis() {
        return numPermis;
    }

    public void setNumPermis(String numPermis) {
        this.numPermis = numPermis;
    }
    
}
