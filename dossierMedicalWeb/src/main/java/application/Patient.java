/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Patient extends AutrePersonne{
    
    
    String villeNaissance;
    String numAssuranceMaladie;

    public Patient(String nom, String prenom, Date dateNaissance, Genre genre, Coordonnees coordonnees, String villeNaissance, String numAssuranceMaladie) {
        
        super(genre,coordonnees, nom, prenom, dateNaissance);
        this.villeNaissance = villeNaissance;
        this.numAssuranceMaladie = numAssuranceMaladie;
    }

    
    public String getVilleNaissance() {
        return villeNaissance;
    }

    public String getNumAssuranceMaladie() {
        return numAssuranceMaladie;
    }

    public void setVilleNaissance(String villeNaissance) {
        this.villeNaissance = villeNaissance;
    }

    public void setNumAssuranceMaladie(String numAssuranceMaladie) {
        this.numAssuranceMaladie = numAssuranceMaladie;
    }
    
    
}
