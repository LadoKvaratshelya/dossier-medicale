/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public abstract class Personnel extends Personne {
    
    String codeEmploye;

    public Personnel(String nom, String prenom, Date dateDeNaissance, String codeEmploye) {
        super(nom, prenom, dateDeNaissance);
        this.codeEmploye = codeEmploye;
    }
    
    
}
