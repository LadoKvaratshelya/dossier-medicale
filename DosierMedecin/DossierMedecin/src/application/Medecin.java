/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author Lado
 */

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

public class Medecin  extends Personnel{
    
    public static Date dateAujourdhui;
    
    private String numPermis;
    private String specialite;

    public Medecin(String codeEmploye, String numPermis, String nom, String prenom, Date dateDeNaissance) {
        super(nom, prenom, dateDeNaissance, codeEmploye);
        this.numPermis = numPermis;
        
    }
    
    public String getNumPermis() {
        return numPermis;
    }

    public void setNumPermis(String numPermis) {
        this.numPermis = numPermis;
    }
    
    public Visite ajouterVisite(Etablissement etablissement, String diagnostique, String resume, Boolean siFin, Boolean siDebut){
        dateAujourdhui = new Date();
        Visite visite = new Visite(etablissement, this, dateAujourdhui, diagnostique, resume, siFin, siDebut);
        return visite;
    }
    
    public Visite addVisite(DossierMedical dossier){
        Etablissement etablissement = null;
        String diagnostique ="";
        String resume = "";
        Boolean siFin = false;
        Boolean siDebut = false;
        dateAujourdhui = new Date();
        Visite visite = new Visite(etablissement, this, dateAujourdhui, diagnostique, resume, siFin, siDebut);
        dossier.getVisites().add(visite);
        
        return visite;
    }
    
    private static String getFormatDateISO(Date date) {
        DateFormat dateFormat;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }
}
