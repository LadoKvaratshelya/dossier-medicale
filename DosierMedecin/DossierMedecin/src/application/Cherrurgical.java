/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Cherrurgical extends Traitement{
    
    private Date dateDeCherrurgie;
    private Enum typeDeAnesthesie;
    private boolean sejourner;
    private Etablissement etablissement;
    private Medecin medecinCherrurgien;

    public Cherrurgical(Date dateDeCherrurgie, boolean sejourner, Etablissement etablissement) {
        this.dateDeCherrurgie = dateDeCherrurgie;
        this.sejourner = sejourner;
        this.etablissement = etablissement;
    }

    public Date getDateDeCherrurgie() {
        return dateDeCherrurgie;
    }

    public void setDateDeCherrurgie(Date dateDeCherrurgie) {
        this.dateDeCherrurgie = dateDeCherrurgie;
    }

    public Enum getTypeDeAnesthesie() {
        return typeDeAnesthesie;
    }

    public void setTypeDeAnesthesie(Enum typeDeAnesthesie) {
        this.typeDeAnesthesie = typeDeAnesthesie;
    }

    public boolean isSejourner() {
        return sejourner;
    }

    public void setSejourner(boolean sejourner) {
        this.sejourner = sejourner;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    public Medecin getMedecinCherrurgien() {
        return medecinCherrurgien;
    }

    public void setMedecinCherrurgien(Medecin medecinCherrurgien) {
        this.medecinCherrurgien = medecinCherrurgien;
    }
    
}
