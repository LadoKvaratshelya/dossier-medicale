/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author Admin
 */
class Medicament{
    private String nomMedicament;
    private String nomMarque;
    private float dose;
    private Enum uniteDeMesure;

    public Medicament(String nomMedicament, float dose) {
        this.nomMedicament = nomMedicament;
        this.dose = dose;
    }

    public String getNomMedicament() {
        return nomMedicament;
    }

    public void setNomMedicament(String nomMedicament) {
        this.nomMedicament = nomMedicament;
    }

    public String getNomMarque() {
        return nomMarque;
    }

    public void setNomMarque(String nomMarque) {
        this.nomMarque = nomMarque;
    }

    public float getDose() {
        return dose;
    }

    public void setDose(float dose) {
        this.dose = dose;
    }

    public Enum getUniteDeMesure() {
        return uniteDeMesure;
    }

    public void setUniteDeMesure(Enum uniteDeMesure) {
        this.uniteDeMesure = uniteDeMesure;
    }
    
    
}
