/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author Admin
 */
class Adresse {
    private long numeroCevic;
    private String nomRue;
    private String ville;
    private String codePostal;
    private String province;

    public Adresse(long numeroCevic, String nomRue, String ville, String codePostal, String province) {
        this.numeroCevic = numeroCevic;
        this.nomRue = nomRue;
        this.ville = ville;
        this.codePostal = codePostal;
        this.province = province;
    }

    public long getNumeroCevic() {
        return numeroCevic;
    }

    public void setNumeroCevic(long numeroCevic) {
        this.numeroCevic = numeroCevic;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    
    
}
