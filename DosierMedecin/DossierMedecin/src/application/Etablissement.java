/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author Admin
 */
public class Etablissement {
    
    public static String types[] = {"public", "prive"};
    public static String statut[] = {"Hopital", "clinique", "polyclinique", "centre hospitalier", "CLSC"};
    
    private String idEtablissement;
    private String nomEtablissement;
    private Coordonnees coordonnees;
    private boolean ouvreFinsDeSemaines;
    private String type;

    public Etablissement(String idEtablissement, String nomEtablissement, boolean ouvreFinsDeSemaines, String type) {
        this.idEtablissement = idEtablissement;
        this.nomEtablissement = nomEtablissement;
        this.ouvreFinsDeSemaines = ouvreFinsDeSemaines;
        this.type = type;
    }

    public static String[] getTypes() {
        return types;
    }

    public static void setTypes(String[] types) {
        Etablissement.types = types;
    }

    public static String[] getStatut() {
        return statut;
    }

    public static void setStatut(String[] statut) {
        Etablissement.statut = statut;
    }

    public String getIdEtablissement() {
        return idEtablissement;
    }

    public void setIdEtablissement(String idEtablissement) {
        this.idEtablissement = idEtablissement;
    }

    public String getNomEtablissement() {
        return nomEtablissement;
    }

    public void setNomEtablissement(String nomEtablissement) {
        this.nomEtablissement = nomEtablissement;
    }

    public Coordonnees getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(Coordonnees coordonnees) {
        this.coordonnees = coordonnees;
    }

    public boolean isOuvreFinsDeSemaines() {
        return ouvreFinsDeSemaines;
    }

    public void setOuvreFinsDeSemaines(boolean ouvreFinsDeSemaines) {
        this.ouvreFinsDeSemaines = ouvreFinsDeSemaines;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
