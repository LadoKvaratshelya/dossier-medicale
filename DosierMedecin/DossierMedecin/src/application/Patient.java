/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class Patient extends AutrePersonne{
    
    
    private String villeNaissance;
    private String numAssuranceMaladie;
    private ArrayList<Parent> parents;
    

    public Patient(String nom, String prenom, Date dateNaissance, Genre genre, Coordonnees coordonnees, String villeNaissance, String numAssuranceMaladie) {
        
        super(genre,coordonnees, nom, prenom, dateNaissance);
        this.villeNaissance = villeNaissance;
        this.numAssuranceMaladie = numAssuranceMaladie;
    }

    
    public String getVilleNaissance() {
        return villeNaissance;
    }

    public String getNumAssuranceMaladie() {
        return numAssuranceMaladie;
    }

    public void setVilleNaissance(String villeNaissance) {
        this.villeNaissance = villeNaissance;
    }

    public void setNumAssuranceMaladie(String numAssuranceMaladie) {
        this.numAssuranceMaladie = numAssuranceMaladie;
    }

    public ArrayList<Parent> getParents() {
        return parents;
    }

    public void setParents(ArrayList<Parent> parents) {
        this.parents = parents;
    }
    
    public boolean addParent(Parent parent){
        return this.parents.add(parent);
    }
    
    public boolean removeParent(Parent parent){
        return this.parents.remove(parent);
    }
    
    public void modifierCoordonnees(Adresse adresse, String courriel, String telephone){
        this.coordonnees.setAdresse(adresse);
        this.coordonnees.setCourriel(courriel);
        this.coordonnees.setTelephone(telephone);
    }
    
    public void accederDossier(DossierMedical dossier){
        
    }
}
