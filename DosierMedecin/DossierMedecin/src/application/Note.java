/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author Admin
 */
class Note {
    private String descriptionNote;
    private Medecin medecinDestine;

    public Note(String descriptionNote, Medecin medecinDestine) {
        this.descriptionNote = descriptionNote;
        this.medecinDestine = medecinDestine;
    }

    public Note(String descriptionNote) {
        this.descriptionNote = descriptionNote;
    }

    public String getDescriptionNote() {
        return descriptionNote;
    }

    public void setDescriptionNote(String descriptionNote) {
        this.descriptionNote = descriptionNote;
    }

    public Medecin getMedecinDestine() {
        return medecinDestine;
    }

    public void setMedecinDestine(Medecin medecinDestine) {
        this.medecinDestine = medecinDestine;
    }
    
    
}
