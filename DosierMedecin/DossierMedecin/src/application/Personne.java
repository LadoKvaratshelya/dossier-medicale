/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;


import java.util.Date;


/**
 *
 * @author Admin
 */
public abstract class Personne {
    
    String nom;
    String prenom;
    Date dateDeNaissance;

    public Personne(String nom, String prenom, Date dateDeNaissance) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
    }
    
    public enum Genre{
        Femme('F', "Mme"),
        Homme('H', "Mr");
        
        private char genre;
        private String titre;
        
        Genre(char genre, String titre){
            this.genre = genre;
            this.titre = titre;
        }

        public char getGenre() {
            return genre;
        }

        public void setGenre(char genre) {
            this.genre = genre;
        }

        public String getTitre() {
            return titre;
        }

        public void setTitre(String titre) {
            this.titre = titre;
        }
        
    }

    
    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public void accederDossier(DossierMedical dossier, Personne personne){}
}

